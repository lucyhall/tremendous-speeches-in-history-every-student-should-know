Writing a speech is one of the essays that you would have to write during your school days. Crafting an [engaging speech](https://advice.writing.utoronto.ca/types-of-writing/public-writing/) is not an easy task, especially when you have no idea how to write a speech. Here are the best incredible speeches in history to help you get some inspiration for your speech writing. These speeches gave hope in despair, lifted hearts in darkness, honored the dead, and refined the characters of men, among other things.

**Charles De Gaulle (The Appeal of 18 June)**
Back in June 1940, it looked like France was losing its country to the German invasion. Prime Minister Paul Reynaud refused to sign an armistice, and therefore he was forced to resign. Marshal Philippe Petain, who succeeded him, stated clearly his intentions to accommodate the Germans.  
General Charles De Gaulle, leader of the Free French Forces, was not pleased with the decision, so he moved to England on June 15. While there, Winston Churchill permitted Charles to make a speech on BBC radio. In his speech, he asked his fellow countrymen not to lose hope and continue fighting against the Germans and Vichy Regime. His speech has inspired me, but at times, I look for someone to help me write a speech.

**Queen Elizabeth 1 1588 ‘Spanish Armada’ Speech to the Troops at Tilbury**
Back in 1788, Queen Elizabeth 1 gave one of the manliest speeches in history. She gave her speech when the Spanish Armada, a flotilla of about 130 ships, sailed towards Britain with invasion plans. 
Fortunately, the storm and some navigational mistakes stopped the attack from happening. Still, Queen delivered a bold speech that helped bolster a nation. It is said that she wore armor in front of her troops.
However, there is no need to be stuck with your speech, not knowing where to begin. At times when I am unable to [write my speech](https://edubirdie.com/write-my-speech), I hire professionals to assist. They can write a fantastic, inspiring, and exciting speech for students facing challenges. They take the assignment workload off your shoulder, and so you get enough time to do other things.

**Martin Luther King’s Speech**
It is one of the most inspiring speeches, mainly because it was given when black Americans were suffering. During the post-abolition times when slavery was outlawed constitutionally, black Americans experienced a difficult period in the hands of white men who supported slavery. 
Martin Luther King gave a speech where he wondered whether racial discrimination would ever be a mare figment where all people would be equal.

**Conclusion**
There are many history speeches. However, these are [some of the best ones in history](https://www.thoughtco.com/how-to-write-a-speech-1857497) that you should read as a student. Therefore, after reading the above speeches, you will get some inspiration and learn how to write captivating speeches.
